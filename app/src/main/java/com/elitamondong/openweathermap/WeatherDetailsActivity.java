package com.elitamondong.openweathermap;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.elitamondong.openweathermap.Model.WeatherResponse;

import org.parceler.Parcels;

import butterknife.BindView;
import butterknife.ButterKnife;


public class WeatherDetailsActivity extends AppCompatActivity {


    WeatherResponse weatherResponse;
    @BindView(R.id.weatherImg)
    ImageView weatherImg;
    @BindView(R.id.location)
    TextView location;
    @BindView(R.id.actualWeather)
    TextView actualWeather;
    @BindView(R.id.temperature)
    TextView temperature;
    @BindView(R.id.coord)
    TextView coord;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.weather_description_layout);
        ButterKnife.bind(this);


//        store = Parcels.unwrap(getIntent().getParcelableExtra("store"));
        weatherResponse = Parcels.unwrap(getIntent().getParcelableExtra("response"));
//
//        getSupportActionBar().setTitle(product.getName());
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setDisplayShowHomeEnabled(true);
//
//        productNameEdt.setText(product.getName());
//        productDescEdt.setText(product.getDescription());
//
//        Log.e("URL",Constants.baseURL + product.getImage().getUrl());
//
        Log.e("ELi", BuildConfig.IMAGE_URL + weatherResponse.getWeather().get(0).getIcon() + ".png");
        Glide.with(this)
                .load(BuildConfig.IMAGE_URL + weatherResponse.getWeather().get(0).getIcon() + ".png")
                .placeholder(R.drawable.ic_luancher)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(weatherImg);

        location.setText(weatherResponse.getName() + "," + weatherResponse.getSys().getCountry());
        actualWeather.setText(weatherResponse.getWeather().get(0).getDescription());
        temperature.setText(weatherResponse.getMain().getTemp() + "C temperature from" + weatherResponse.getMain().getTempMin() + " to " + weatherResponse.getMain().getTempMax() + "C");
        coord.setText("[" + weatherResponse.getCoord().getLon() + "," + weatherResponse.getCoord().getLat() + "]");


//        productTotalBillEdt.setText(product.getDiscount());

//        productTotalBillEdt.addTextChangedListener(new TextWatcherForThousand(productTotalBillEdt));
    }


//    @OnClick(R.id.scanCodeButton)
//    public void scanQR() {
//        if (store != null || product != null)
//        if (productTotalBillEdt.getText().toString().isEmpty()) {
//            productTotalBillEdt.setError("Please enter your total bill");
//        } else {
//            Bundle bundle = new Bundle();
//            Intent intent = new Intent(ProductDetailsActivity.this, ScannerActivity.class);
//            bundle.putParcelable("product", Parcels.wrap(product));
//            bundle.putParcelable("store", Parcels.wrap(store));
//            bundle.putString("total_bill", productTotalBillEdt.getText().toString());
//            intent.putExtras(bundle);
//            startActivity(intent);
//        }
//
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                // app icon in action bar clicked; go home
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


}
