package com.elitamondong.openweathermap.Utilities;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.androidadvance.topsnackbar.TSnackbar;
import java.text.ParseException;
import java.text.SimpleDateFormat;


/**
 * Created by eleazartamondong on 08/06/2018.
 */

public class CustomUtils extends AppCompatActivity {

    Context context;
    public static ProgressDialog progress;


//    public void loadFragment(final Fragment fragment, boolean addToBackStack, Context context, String tag) {
//        final FragmentTransaction fragmentTransaction = ((FragmentActivity) context).getSupportFragmentManager()
//                .beginTransaction()
//                .replace(R.id.container, fragment, tag);
//        if (addToBackStack) {
//            fragmentTransaction.addToBackStack(tag);
//        }
////        CustomUtils.popBackStack(fragment);
//        fragmentTransaction.commit();
//    }

    public boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    public void showErrorMessage(View view,String message){

        TSnackbar snackbar = TSnackbar.make(view, message, TSnackbar.LENGTH_LONG);
        snackbar.setActionTextColor(Color.WHITE);
        View snackbarView = snackbar.getView();
        snackbarView.setBackgroundColor(Color.RED);
        snackbar.show();


    }


    
    public String formatTime(String timeToFormat){

        String time24 = "00:00";
                
        try {
        SimpleDateFormat inFormat = new SimpleDateFormat("hh:mm aa");
        SimpleDateFormat outFormat = new SimpleDateFormat("HH:mm");
        
          time24  = outFormat.format(inFormat.parse(timeToFormat));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return time24;
        
    }
}
