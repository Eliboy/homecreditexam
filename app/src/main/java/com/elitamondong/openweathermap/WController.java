package com.elitamondong.openweathermap;

import android.app.Application;

import com.elitamondong.openweathermap.Utilities.ConnectivityReceiver;
import com.facebook.stetho.Stetho;
import com.uphyca.stetho_realm.RealmInspectorModulesProvider;

import io.realm.Realm;
import io.realm.RealmConfiguration;



public class WController extends Application {
    private static WController mInstance;

    /**
     * This method used to get the instance of the application.
     */
    public static synchronized WController getInstance() {
        return mInstance;
    }

    /**
     * Method to set the connectivity listener
     */
    public void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener) {
        ConnectivityReceiver.connectivityReceiverListener = listener;
    }

    @Override
    public void onCreate(){
        super.onCreate();
        Realm.init(this);
        mInstance = this;
        /**
         * Realm Configuration
         */
        RealmConfiguration config = new RealmConfiguration
                .Builder()
                .schemaVersion(25)
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(config);

        /**
         * Stetho Initializer. Comment this in production
         */
        Stetho.initialize(
                Stetho.newInitializerBuilder(this)
                        .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
                        .enableWebKitInspector(RealmInspectorModulesProvider.builder(this).build())
                        .build());


    }

}
