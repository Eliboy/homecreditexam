package com.elitamondong.openweathermap.Networking;

import com.elitamondong.openweathermap.Model.Cnt;
import com.elitamondong.openweathermap.Model.WeatherResponse;

import java.util.HashMap;


import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by eleazartamondong on 08/06/2018.
 */

public interface APIInterface {



    //RxJava
    @GET("data/2.5/group")
    Observable<Cnt> getWeather(@Query("id") String ids,
                               @Query("units") String units,
                               @Query("appid") String appID);



}
