package com.elitamondong.openweathermap;

import android.util.Log;

import com.elitamondong.openweathermap.Model.Cnt;
import com.elitamondong.openweathermap.Model.WeatherResponse;
import com.elitamondong.openweathermap.Networking.APIInterface;
import com.elitamondong.openweathermap.Networking.OnApiRequestListener;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by EliVT
 */

public class ApiRequestHelper {

    private OnApiRequestListener onApiRequestListener;
    private APIInterface apiInterface;

    public ApiRequestHelper(OnApiRequestListener onApiRequestListener, APIInterface apiInterface) {
        this.onApiRequestListener = onApiRequestListener;
        this.apiInterface = apiInterface;
    }

    /**
     * handle api result using lambda
     *
     * @param action     identification of the current api request
     * @param observable actual process of the api request
     */
    private void handleObservableResult(final ApiAction action, final Observable observable) {

        observable.observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(result -> onApiRequestListener.onApiRequestSuccess(action, result),
                        throwable -> onApiRequestListener.onApiRequestFailed(action, (Throwable) throwable),
                        () -> Log.e("api", "api request completed --> " + action));
    }



    /****************
     * API METHOD CALLS
     *****************/



    public void getWeather(String ids) {
        onApiRequestListener.onApiRequestStart(ApiAction.GETWEATHER);
        final Observable<Cnt> observable = apiInterface.getWeather(ids,"metric",BuildConfig.SECRET_KEY);
        handleObservableResult(ApiAction.GETWEATHER, observable);
    }



}
