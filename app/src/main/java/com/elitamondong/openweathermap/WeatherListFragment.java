package com.elitamondong.openweathermap;

import android.content.Intent;
import android.os.Bundle;

import android.app.Fragment;

import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.MaterialDialog;
import com.elitamondong.openweathermap.Adapter.WeatherAdapter;
import com.elitamondong.openweathermap.Model.Cnt;
import com.elitamondong.openweathermap.Model.WeatherResponse;
import com.elitamondong.openweathermap.Presenter.Weather.WeatherPresenter;
import com.elitamondong.openweathermap.Presenter.Weather.WeatherPresenterImpl;
import com.elitamondong.openweathermap.Utilities.CustomUtils;
import com.elitamondong.openweathermap.Utilities.RecyclerTouchListener;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;


/**
 * Created by eleazartamondong on 08/06/2018.
 */

public class WeatherListFragment extends Fragment implements WeatherPresenter.View{

    @BindView(R.id.RecyclerView)
    RecyclerView weatherRecyclerView;
    MaterialDialog materialDialog;

    WeatherPresenter weatherPresenter;
    WeatherAdapter weatherAdapter;
    List<WeatherResponse> weatherResponseList;
    CustomUtils customUtils = new CustomUtils();

    Realm realm;
    RealmQuery<WeatherResponse> weatherResponseRealmQuery;
    RealmResults<WeatherResponse> weatherResponseRealmResults;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getActivity().setTitle("Weather");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.list_recyable_layout, container, false);
        ButterKnife.bind(this, view);
        weatherResponseList = new ArrayList<>();

        weatherPresenter = new WeatherPresenterImpl(this, getActivity());
        realm = Realm.getDefaultInstance();

        refreshList();

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        weatherRecyclerView.setLayoutManager(mLayoutManager);
        weatherRecyclerView.setItemAnimator(new DefaultItemAnimator());
        weatherRecyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), new RecyclerTouchListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        WeatherResponse wr = weatherResponseRealmResults == null ? weatherResponseList.get(position) : weatherResponseRealmResults.get(position);
                        Bundle bundle = new Bundle();
                        Intent intent = new Intent(getActivity(), WeatherDetailsActivity.class);
                        bundle.putParcelable("response", Parcels.wrap(wr));
                        intent.putExtras(bundle);
                        startActivity(intent);
                    }
                })
        );
        return view;
    }

    @Override
    public void setWeather(Cnt responses) {

        List<WeatherResponse> wr = (ArrayList<WeatherResponse>) responses.getList();

        weatherResponseList.clear();
        weatherResponseList.addAll(wr);
        weatherAdapter = new WeatherAdapter(getActivity(), weatherResponseList);
        weatherRecyclerView.setAdapter(weatherAdapter);
    }

    @Override
    public void showProgress() {
        MaterialDialog.Builder builder = new MaterialDialog.Builder(getActivity());
        builder.title("Loading Weather Locations");
        builder.content("Please wait...");
        builder.progress(true, 0);
        materialDialog = builder.build();
        materialDialog.show();
    }

    @Override
    public void hideProgress() {
        materialDialog.dismiss();
    }

    @Override
    public void setCodeError(String message) {

    }


    public void refreshList() {
        if (customUtils.isNetworkAvailable(getActivity())) {

            //ID for LONDON , PRAGUE , SAN FRANCISCO
            weatherPresenter.loadWeather("2643743,3067696,5391959");
        } else {
            weatherResponseRealmQuery = realm.where(WeatherResponse.class);
            weatherResponseRealmResults = weatherResponseRealmQuery.findAll();
            weatherAdapter = new WeatherAdapter(getActivity(), weatherResponseRealmResults);
            weatherRecyclerView.setAdapter(weatherAdapter);
        }
    }
}
