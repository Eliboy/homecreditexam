package com.elitamondong.openweathermap;

import android.content.Context;
import android.os.Bundle;

import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by eleazartamondong on 08/06/2018.
 */

public class RefreshFragment extends Fragment {

    @BindView(R.id.refresh)
    Button refresh;


    private OnItemSelectedListener listener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    //
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_button, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @OnClick(R.id.refresh)
    public void refreshList(){
        listener.onRssItemSelected();

    }

    public interface OnItemSelectedListener {
        void onRssItemSelected();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnItemSelectedListener) {
            listener = (OnItemSelectedListener) context;
        } else {
            throw new ClassCastException(context.toString()
                    + " must implemenet WeatherListFragment.OnItemSelectedListener");
        }
    }

}
