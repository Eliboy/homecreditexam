package com.elitamondong.openweathermap.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.elitamondong.openweathermap.Model.WeatherResponse;
import com.elitamondong.openweathermap.R;

import java.util.List;


/**
 * Created by vidalbenjoe on 08/03/2017.
 */

public class WeatherAdapter extends RecyclerView.Adapter<WeatherAdapter.MyViewHolder> {
    List<WeatherResponse> weatherResponseList;
    Context context;

    public WeatherAdapter(Context context, List<WeatherResponse> weatherResponses) {
        this.context = context;
        this.weatherResponseList = weatherResponses;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.weather_card_layout, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        WeatherResponse wr = weatherResponseList.get(position);
        holder.location.setText(wr.getName() + "," + wr.getSys().getCountry());
        holder.actualweather.setText(wr.getWeather().get(0).getDescription());
        holder.temperature.setText(wr.getMain().getTemp() + "C temperature from" + wr.getMain().getTempMin() + " to " + wr.getMain().getTempMax() + "C");
        holder.coord.setText("[" + wr.getCoord().getLon() + "," + wr.getCoord().getLat() + "]");


//        Glide.with(context)
//                .load(Constants.baseURL + news.getImage().getUrl())
//                .centerCrop()
//                .placeholder(R.drawable.ic_no_preview)
//                .diskCacheStrategy(DiskCacheStrategy.ALL)
//                .dontAnimate()
//                .into(holder.thumbnail);

    }

    @Override
    public int getItemCount() {
        return weatherResponseList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView location;
        public TextView actualweather;
        public TextView temperature;
        public TextView coord;

        public MyViewHolder(View view) {
            super(view);

            location = (TextView) view.findViewById(R.id.location);
            actualweather = (TextView) view.findViewById(R.id.actualWeather);
            temperature = (TextView) view.findViewById(R.id.temperature);
            coord = (TextView) view.findViewById(R.id.coord);
//            thumbnail = (ImageView) view.findViewById(R.id.thumbnail);
        }
    }

}
