package com.elitamondong.openweathermap.Presenter.Weather;

import android.content.Context;

import com.elitamondong.openweathermap.ApiRequestHelper;
import com.elitamondong.openweathermap.Networking.APIClient;
import com.elitamondong.openweathermap.Networking.APIInterface;
import com.elitamondong.openweathermap.Networking.OnApiRequestListener;


/**
 * Created by eleazartamondong on 08/06/2018.
 */

public class WeatherInteractorImpl implements WeatherInteractor {
    Context context;
    ApiRequestHelper apiRequestHelper;
    public WeatherInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void getWeather(final String ids,final OnApiRequestListener listener) {


        APIInterface apiService =
                APIClient.getClient(context).create(APIInterface.class);
        APIClient.isHasAuthorization = false;

        apiRequestHelper = new ApiRequestHelper(listener,apiService);

        apiRequestHelper.getWeather(ids);

     }
}
