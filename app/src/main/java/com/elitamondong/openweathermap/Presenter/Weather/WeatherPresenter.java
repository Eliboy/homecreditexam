package com.elitamondong.openweathermap.Presenter.Weather;

import com.elitamondong.openweathermap.Model.Cnt;
import com.elitamondong.openweathermap.Model.WeatherResponse;

import java.util.List;


/**
 * Created by eleazartamondong on 08/06/2018.
 */

public interface WeatherPresenter {
    interface View{

        void setWeather(Cnt responses);

        void showProgress();

        void hideProgress();

        void setCodeError(String message);
    }
    void loadWeather(String ids);
}
