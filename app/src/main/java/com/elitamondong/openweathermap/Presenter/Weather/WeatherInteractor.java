package com.elitamondong.openweathermap.Presenter.Weather;


import com.elitamondong.openweathermap.Networking.OnApiRequestListener;

/**
 * Created by eleazartamondong on 08/06/2018.
 */

public interface WeatherInteractor {



    void getWeather(String ids,OnApiRequestListener listener);

}
