package com.elitamondong.openweathermap.Presenter.Weather;

import android.content.Context;
import android.util.Log;

import com.elitamondong.openweathermap.ApiAction;
import com.elitamondong.openweathermap.Model.Cnt;
import com.elitamondong.openweathermap.Model.WeatherResponse;
import com.elitamondong.openweathermap.Networking.OnApiRequestListener;
import com.elitamondong.openweathermap.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import retrofit2.adapter.rxjava.HttpException;

/**
 * Created by Eli tamondong on 04/03/2018.
 */

public class WeatherPresenterImpl implements WeatherPresenter, OnApiRequestListener {

    View weatherView;
    WeatherInteractorImpl weatherInteractor;
    Context context;

    Realm realm;

    public WeatherPresenterImpl(View view, Context context) {
        this.weatherView = view;
        this.weatherInteractor = new WeatherInteractorImpl(context);
        this.context = context;

        realm = Realm.getDefaultInstance();
    }

    @Override
    public void loadWeather(String ids) {

        weatherInteractor.getWeather(ids,this);
    }


    @Override
    public void onApiRequestStart(ApiAction apiAction) {

        if (apiAction.equals(ApiAction.GETWEATHER)) {
            weatherView.showProgress();
        }
    }

    @Override
    public void onApiRequestFailed(ApiAction apiAction, Throwable t) {


        if (t.getMessage().contains(context.getResources().getString(R.string.msg_contains_failedtoconnect))) {
            weatherView.setCodeError(context.getResources().getString(R.string.msg_warn_networkerror));

        } else if (t.getMessage().contains(context.getResources().getString(R.string.msg_contains_timeout))) {

            weatherView.setCodeError(context.getResources().getString(R.string.msg_warn_networkerror));
        } else {
            if (t instanceof HttpException) {
                final HttpException ex = (HttpException) t;
                if (ex.code() == 500) {
                    //server related issue

                    weatherView.setCodeError(context.getResources().getString(R.string.msg_warn_servererror));
                } else {

                    HttpException error = (HttpException)t;
                    try {
                        String errorBody = error.response().errorBody().string();
                        JSONObject jObjError = new JSONObject(errorBody);

                        Log.e("MESSAGE1", "--- " + errorBody.toString());
                        weatherView.setCodeError(jObjError.getString("error_description"));
                    } catch (IOException e) {
                        e.printStackTrace();
                        Log.e("MESSAGE2", "--- ");
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.e("MESSAGE3", "--- " + e.getMessage());
                    }

                }
            } else if (t instanceof IOException) {

                weatherView.setCodeError(context.getResources().getString(R.string.msg_warn_networkerror));
            }
        }
    }

    @Override
    public void onApiRequestSuccess(ApiAction apiAction, Object result) {

        if (apiAction.equals(ApiAction.GETWEATHER)){
            Cnt response = (Cnt) result;



                weatherView.setWeather(response);
                weatherView.hideProgress();

//                Realm realm = Realm.getDefaultInstance();
//                realm.beginTransaction();
//                realm.copyToRealmOrUpdate(rooms);
//                realm.commitTransaction();


            Realm realm = Realm.getDefaultInstance();
            realm.beginTransaction();
            realm.insertOrUpdate(response.getList());
            realm.commitTransaction();

        }
    }
}
