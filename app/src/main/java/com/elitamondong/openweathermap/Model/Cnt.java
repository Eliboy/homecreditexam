package com.elitamondong.openweathermap.Model;

/**
 * Created by eleazartamondong on 08/06/2018.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Cnt{

    @SerializedName("cnt")
    @Expose
    @PrimaryKey
    private Integer cnt;
    @SerializedName("list")
    @Expose
    private List<WeatherResponse> responses;

    public Integer getCnt() {
        return cnt;
    }

    public void setCnt(Integer cnt) {
        this.cnt = cnt;
    }

    public List<WeatherResponse>  getList() {
        return responses;
    }

    public void setList(List<WeatherResponse>  list) {
        this.responses = list;
    }

}
